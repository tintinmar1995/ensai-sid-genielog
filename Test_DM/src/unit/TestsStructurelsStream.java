package unit;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestsStructurelsStream {
	

	private String[] arg = new String[1];
	private Random mRandom = new Random();
	private Stream mStream;

	@BeforeEach
	void setUp() {
		arg[0]="/home/martin/eclipse-workspace/Test_DM/tmp2.txt";
		try {
	        PrintWriter targetFile = new PrintWriter(new FileWriter("./tmp2.txt"));
	        for(int i=0;i<100;i++){
	                targetFile.write(Math.abs(mRandom.nextInt())+";"+Math.abs(mRandom.nextInt()));
	        }
	        targetFile.close();
			mStream = new Stream(arg[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void TestParseLine() {
		int a,b;
		for(int i=0;i<100;i++) {
			a = Math.abs(mRandom.nextInt());
			b = Math.abs(mRandom.nextInt());
			if(mStream.parseLine(a+";"+b)!=Double.parseDouble(Double.toString(a/b)))
				assertTrue(false);			
		}
	}
	
}
	