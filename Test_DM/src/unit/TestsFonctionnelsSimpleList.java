package unit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

class TestsFonctionnelsSimpleList {
	
	private SimpleList mSimpleList;
	SimpleList mSimpleListEmpty;
	private Random mRandom = new Random();
	private int nbTest = 9;
	
	/* 
	 * Avant chaque test, on initialise mSimpleList, objet sur lequel les tests vont être menés
	 * On n'ajoute aucune valeur dedans car la méthode add n'a pas encore été testée et qu'il est utile pour les tests
	 * aux limites d'avoir une liste vide.
	 * */
	@BeforeEach
	void setUp() {
		mSimpleList = new SimpleList();
		nbTest = mRandom.nextInt();
		for (int i=0;i<Math.abs(mRandom.nextInt(18)-9);i++) {
			mSimpleList.add(mRandom.nextInt());
		}
		
		
		mSimpleListEmpty = new SimpleList();
	}
	
	/* 
	 * On crée une fonction qui sera utile pour générer des tests. Cette fonction permet de remplir presque complétement une liste 
	 */
	SimpleList goToMaxMoinsUn(SimpleList debut,int ajout) {
		for(int i=debut.length();i<SimpleList.taillemax-1;i++) {
			debut.add(ajout);
		}
		return debut;
	}
	
	/*
	 * Ce premier test teste les méthodes add et length dans le même temps N fois.
	 * On teste ensuite avec des valeurs fixées. Il parait inutile de générer aléatoirement les valeurs. 
	 */
	@Test
	void testLengthAndAddLim0() {
		mSimpleListEmpty.add(1);
		assertEquals(mSimpleListEmpty.length(),1);
		
	}

	
	/*
	 * La fonction Current doit retourner le dernier élément de la liste
	 * On teste ici pour une valeur fixée en initialisant la liste via le setUp() du monde de test
	 */
	@Test
	void testCurrent() {
		mSimpleList.add(1);	
		mSimpleList.add(2);
		mSimpleList.add(3);
		int valeurATest = 9;	
		mSimpleList.add(valeurATest);
		assertEquals(mSimpleList.current(),valeurATest);
	}
	
	
	@Test
	void testGet() {
		int tailleEssai;
		int[] essai;
		int temp;
		
		for(int k=0;k<20;k++) {		
			tailleEssai = Math.abs(mRandom.nextInt(18)-9);
			essai = new int[tailleEssai];
			
			for (int i=0;i<tailleEssai;i++) {
				temp = mRandom.nextInt();
				essai[i]= temp;
				mSimpleListEmpty.add(temp);
			}
			
			for(int j=0;j<nbTest;j++) {
				temp = Math.abs(mRandom.nextInt(2*tailleEssai-2)-tailleEssai+1);
				assertEquals(mSimpleListEmpty.get(temp),essai[temp]);
			}
		}
	}
	
	
	/*
	 * On test la possibilité d'utiliser Length sur une liste vide
	 */
	@Test
	void testLengthOnEmptyList() {	
		assertEquals(mSimpleListEmpty.length(),0);
	}
	
	/*
	 * Ce test vérifie que l'on ne peut pas ajouter plus de valeurs que la longueur maximale fixée
	 */
	@Test
	void testAddMoreThanMaxExcept() {		
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> {
			for(int i=1;i<SimpleList.taillemax+3;i++){
				mSimpleList.add(1);	
			}	
		});
	}
	
	/*
	 * Ce test vérifie que l'on ne peut pas ajouter plus de valeurs que la longueur maximale fixée
	 */
	@Test
	void testGetOverLimit() {
		mSimpleListEmpty.add(1);
		mSimpleListEmpty.add(1);
		assertEquals(mSimpleListEmpty.get(mSimpleListEmpty.length()+1),null);
	}
	
	@Test
	void testIntegriteApresErreur() {		
		goToMaxMoinsUn(mSimpleList,1);
		
		try {
			mSimpleList.add(111);	
			mSimpleList.add(111);	
		}catch(IndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		assertEquals(mSimpleListEmpty.length(),SimpleList.taillemax);			
		assertEquals(mSimpleListEmpty.current(),SimpleList.taillemax-1);			
	}
	
	/*
	 * Ce second test teste aux limites
	 * Ce test dépend d'une valeur numérique fixée (la taille du tableau dans la classe SimpleList)
	 * J'ai ainsi proposé une modification de la classe SimpleList en ajoutant en static la longueur maximal d'une liste, on peut dès à présent adpater les tests à la taille max 
	 */
	@Test
	void testLimitLength() {	
		assertEquals(mSimpleListEmpty.length(),0);
		mSimpleListEmpty = goToMaxMoinsUn(mSimpleListEmpty,5);
		assertEquals(mSimpleListEmpty.length(),SimpleList.taillemax-1);
		mSimpleListEmpty.add(SimpleList.taillemax);
		assertEquals(mSimpleListEmpty.length(),SimpleList.taillemax);
		}

	/*
	 * Ce test teste la méthode current aux limites. 
	 */
	@Test
	void testLimiteCurrent0() {
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> {
				mSimpleListEmpty.current();	
		});
		
		}
	

	/*
	 * Ce test teste la méthode current aux limites. 
	 */
	@Test
	void testLimiteCurrentMax() {
		mSimpleListEmpty = goToMaxMoinsUn(mSimpleListEmpty,5);
		mSimpleListEmpty.add(50);
		assertEquals(mSimpleListEmpty.current(),50);	
	}
	
	
	
	@Test
	void testIterate() {
		
		SimpleList mListeRead = new SimpleList();
		try {
			mSimpleList.iterate();
			BufferedReader br = new BufferedReader(new FileReader("tmp.txt"));
			try {
			    String line = br.readLine();
			    while (line != null & !line.equals("Nothing here ")) {
			    	mListeRead.add(Integer.parseInt(line));
			        line = br.readLine();
			    }
			} finally {
			    br.close();
			}
			
			for(int i=0;i<mListeRead.length();i++) {
				assertEquals(mListeRead.get(i),mSimpleList.get(i));
			}
			
		} catch (IOException | IndexOutOfBoundsException e) {
			e.printStackTrace();
		}
				
	}
	
	@Test
	void testIterateSameLength() {
		
		SimpleList mListeRead = new SimpleList();
		mSimpleList.add(1);
		mSimpleList.add(0);
		mSimpleList.add(2);
				
		try {
			mSimpleList.iterate();
			
			BufferedReader br = new BufferedReader(new FileReader("tmp.txt"));
			try {
			    String line = br.readLine();
			    while (line != null & !line.equals("Nothing here ")) {
			    	mListeRead.add(Integer.parseInt(line));
			        line = br.readLine();
			    }
			} finally {
			    br.close();
			}
			
			for(int i=0;i<mSimpleList.length();i++) {
				assertEquals(mListeRead.get(i),mSimpleList.get(i));
			}
			
		} catch (IOException | IndexOutOfBoundsException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			assertTrue(false);
		}
				
	}
	
}
