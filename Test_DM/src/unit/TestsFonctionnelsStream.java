package unit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.Assertions;

public class TestsFonctionnelsStream {

	private String[] arg = new String[1];
	private Random mRandom = new Random();
    private  List<Double> prevision;

	@BeforeEach
	void setUp() {
		int a,b;
		arg[0]="/home/martin/eclipse-workspace/Test_DM/tmp2.txt";
		try {
	        PrintWriter targetFile = new PrintWriter(new FileWriter("./tmp2.txt"));
	        for(int i=0;i<100;i++){
	        		a = Math.abs(mRandom.nextInt());
	        		b= Math.abs(mRandom.nextInt());
	        		prevision.add(Double.parseDouble(Double.toString(a/b)));
	                targetFile.write(a+";"+b+"\n");
	        }
	        targetFile.write(Math.abs(mRandom.nextInt())+";"+0+"\n");
	        targetFile.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void testRead() {
		try {
			Stream.main(arg);
		}catch(Exception e) {
			assertTrue(false);
		}
	}
	
	
	@Test
	void testCorrect() {
		try {
			List<Double> res = Stream.main2(arg[0]);
			if(res.size()==prevision.size()) {
				for(int i=0;i<Stream.resultat.size();i++) {
					if(Stream.resultat.get(i)!=prevision.get(i))
						assertTrue(false);
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
}
