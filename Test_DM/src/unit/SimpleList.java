package unit;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class SimpleList {

	public static int taillemax = 10;
    private int[] tab = new int[taillemax];
    private int current = -1;

    public void add(int i) {
            current++;
            tab[current]=i;
    }

    public int length() {
        return current+1;
    }

    public int current() {
        return tab[current];
    }

    public int get(int i) {
        return tab[i];
    }

    public void iterate() throws IOException {
        PrintWriter targetFile = new PrintWriter(new FileWriter("./tmp.txt"));
        for(int i=0;i<tab.length;i++){
            if (i>current){
                targetFile.write("Nothing here \n");
            }else{
            	targetFile.write(tab[i]+"\n");
            }
        }
        targetFile.close();
    }
}