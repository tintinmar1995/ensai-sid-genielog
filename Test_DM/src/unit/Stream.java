package unit;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class Stream {
    BufferedReader reader;
    public static List<Double> resultat;
    

    public Stream(String fileNameData) throws FileNotFoundException {
        this.reader = new BufferedReader(new FileReader(fileNameData));
    }
    
    public void readData()  {
        try {
            String line = reader.readLine();
            while (line != null) {
                System.out.println(parseLine(line));
                line = reader.readLine();
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    protected double parseLine(String line) {
        String[] split = line.split(";");
        double temp = Double.parseDouble(split[0]) / Double.parseDouble(split[1]);
        resultat.add(temp);
        return temp;
    }
    
    public static void main(String[] args) throws FileNotFoundException {
        Stream s = new Stream(args[0]);
        s.readData();
    }

    public static List<Double> main2(String str) throws FileNotFoundException {
        Stream s = new Stream(str);
        s.readData();
        return resultat;
    }
    
}
