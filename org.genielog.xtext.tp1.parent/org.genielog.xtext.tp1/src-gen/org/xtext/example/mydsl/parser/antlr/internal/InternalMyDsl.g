/*
 * generated by Xtext 2.16.0
 */
grammar InternalMyDsl;

options {
	superClass=AbstractInternalAntlrParser;
}

@lexer::header {
package org.xtext.example.mydsl.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package org.xtext.example.mydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;

}

@parser::members {

 	private MyDslGrammarAccess grammarAccess;

    public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
        this(input);
        this.grammarAccess = grammarAccess;
        registerRules(grammarAccess.getGrammar());
    }

    @Override
    protected String getFirstRuleName() {
    	return "VideoGen";
   	}

   	@Override
   	protected MyDslGrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}

}

@rulecatch {
    catch (RecognitionException re) {
        recover(input,re);
        appendSkippedTokens();
    }
}

// Entry rule entryRuleVideoGen
entryRuleVideoGen returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getVideoGenRule()); }
	iv_ruleVideoGen=ruleVideoGen
	{ $current=$iv_ruleVideoGen.current; }
	EOF;

// Rule VideoGen
ruleVideoGen returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='Videogen'
		{
			newLeafNode(otherlv_0, grammarAccess.getVideoGenAccess().getVideogenKeyword_0());
		}
		otherlv_1='{'
		{
			newLeafNode(otherlv_1, grammarAccess.getVideoGenAccess().getLeftCurlyBracketKeyword_1());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getVideoGenAccess().getVidsVideoSequenceParserRuleCall_2_0());
				}
				lv_vids_2_0=ruleVideoSequence
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getVideoGenRule());
					}
					add(
						$current,
						"vids",
						lv_vids_2_0,
						"org.xtext.example.mydsl.MyDsl.VideoSequence");
					afterParserOrEnumRuleCall();
				}
			)
		)+
		otherlv_3='}'
		{
			newLeafNode(otherlv_3, grammarAccess.getVideoGenAccess().getRightCurlyBracketKeyword_3());
		}
	)
;

// Entry rule entryRuleVideoSequence
entryRuleVideoSequence returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getVideoSequenceRule()); }
	iv_ruleVideoSequence=ruleVideoSequence
	{ $current=$iv_ruleVideoSequence.current; }
	EOF;

// Rule VideoSequence
ruleVideoSequence returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		{
			newCompositeNode(grammarAccess.getVideoSequenceAccess().getMandatoryParserRuleCall_0());
		}
		this_mandatory_0=rulemandatory
		{
			$current = $this_mandatory_0.current;
			afterParserOrEnumRuleCall();
		}
		    |
		{
			newCompositeNode(grammarAccess.getVideoSequenceAccess().getOptionalParserRuleCall_1());
		}
		this_optional_1=ruleoptional
		{
			$current = $this_optional_1.current;
			afterParserOrEnumRuleCall();
		}
		    |
		{
			newCompositeNode(grammarAccess.getVideoSequenceAccess().getAlternativesParserRuleCall_2());
		}
		this_alternatives_2=rulealternatives
		{
			$current = $this_alternatives_2.current;
			afterParserOrEnumRuleCall();
		}
	)
;

// Entry rule entryRulemandatory
entryRulemandatory returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getMandatoryRule()); }
	iv_rulemandatory=rulemandatory
	{ $current=$iv_rulemandatory.current; }
	EOF;

// Rule mandatory
rulemandatory returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='mandatory'
		{
			newLeafNode(otherlv_0, grammarAccess.getMandatoryAccess().getMandatoryKeyword_0());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getMandatoryAccess().getVidVideoseqParserRuleCall_1_0());
				}
				lv_vid_1_0=rulevideoseq
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getMandatoryRule());
					}
					set(
						$current,
						"vid",
						lv_vid_1_0,
						"org.xtext.example.mydsl.MyDsl.videoseq");
					afterParserOrEnumRuleCall();
				}
			)
		)
	)
;

// Entry rule entryRuleoptional
entryRuleoptional returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getOptionalRule()); }
	iv_ruleoptional=ruleoptional
	{ $current=$iv_ruleoptional.current; }
	EOF;

// Rule optional
ruleoptional returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='optional'
		{
			newLeafNode(otherlv_0, grammarAccess.getOptionalAccess().getOptionalKeyword_0());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getOptionalAccess().getVidVideoseqParserRuleCall_1_0());
				}
				lv_vid_1_0=rulevideoseq
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getOptionalRule());
					}
					set(
						$current,
						"vid",
						lv_vid_1_0,
						"org.xtext.example.mydsl.MyDsl.videoseq");
					afterParserOrEnumRuleCall();
				}
			)
		)
	)
;

// Entry rule entryRulealternatives
entryRulealternatives returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getAlternativesRule()); }
	iv_rulealternatives=rulealternatives
	{ $current=$iv_rulealternatives.current; }
	EOF;

// Rule alternatives
rulealternatives returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='alternatives'
		{
			newLeafNode(otherlv_0, grammarAccess.getAlternativesAccess().getAlternativesKeyword_0());
		}
		(
			(
				lv_id_1_0=RULE_ID
				{
					newLeafNode(lv_id_1_0, grammarAccess.getAlternativesAccess().getIdIDTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getAlternativesRule());
					}
					setWithLastConsumed(
						$current,
						"id",
						lv_id_1_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
		otherlv_2='{'
		{
			newLeafNode(otherlv_2, grammarAccess.getAlternativesAccess().getLeftCurlyBracketKeyword_2());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getAlternativesAccess().getVidsVideoseqParserRuleCall_3_0());
				}
				lv_vids_3_0=rulevideoseq
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getAlternativesRule());
					}
					add(
						$current,
						"vids",
						lv_vids_3_0,
						"org.xtext.example.mydsl.MyDsl.videoseq");
					afterParserOrEnumRuleCall();
				}
			)
		)+
	)
;

// Entry rule entryRulevideoseq
entryRulevideoseq returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getVideoseqRule()); }
	iv_rulevideoseq=rulevideoseq
	{ $current=$iv_rulevideoseq.current; }
	EOF;

// Rule videoseq
rulevideoseq returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='videoseq'
		{
			newLeafNode(otherlv_0, grammarAccess.getVideoseqAccess().getVideoseqKeyword_0());
		}
		(
			(
				lv_id_1_0=RULE_ID
				{
					newLeafNode(lv_id_1_0, grammarAccess.getVideoseqAccess().getIdIDTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getVideoseqRule());
					}
					setWithLastConsumed(
						$current,
						"id",
						lv_id_1_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
		(
			(
				lv_url_2_0=RULE_STRING
				{
					newLeafNode(lv_url_2_0, grammarAccess.getVideoseqAccess().getUrlSTRINGTerminalRuleCall_2_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getVideoseqRule());
					}
					setWithLastConsumed(
						$current,
						"url",
						lv_url_2_0,
						"org.eclipse.xtext.common.Terminals.STRING");
				}
			)
		)
	)
;

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;
