package org.xtext.example.mydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Videogen'", "'{'", "'}'", "'mandatory'", "'optional'", "'alternatives'", "'videoseq'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int RULE_INT=6;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }



     	private MyDslGrammarAccess grammarAccess;

        public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "VideoGen";
       	}

       	@Override
       	protected MyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleVideoGen"
    // InternalMyDsl.g:64:1: entryRuleVideoGen returns [EObject current=null] : iv_ruleVideoGen= ruleVideoGen EOF ;
    public final EObject entryRuleVideoGen() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVideoGen = null;


        try {
            // InternalMyDsl.g:64:49: (iv_ruleVideoGen= ruleVideoGen EOF )
            // InternalMyDsl.g:65:2: iv_ruleVideoGen= ruleVideoGen EOF
            {
             newCompositeNode(grammarAccess.getVideoGenRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVideoGen=ruleVideoGen();

            state._fsp--;

             current =iv_ruleVideoGen; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVideoGen"


    // $ANTLR start "ruleVideoGen"
    // InternalMyDsl.g:71:1: ruleVideoGen returns [EObject current=null] : (otherlv_0= 'Videogen' otherlv_1= '{' ( (lv_vids_2_0= ruleVideoSequence ) )+ otherlv_3= '}' ) ;
    public final EObject ruleVideoGen() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_vids_2_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:77:2: ( (otherlv_0= 'Videogen' otherlv_1= '{' ( (lv_vids_2_0= ruleVideoSequence ) )+ otherlv_3= '}' ) )
            // InternalMyDsl.g:78:2: (otherlv_0= 'Videogen' otherlv_1= '{' ( (lv_vids_2_0= ruleVideoSequence ) )+ otherlv_3= '}' )
            {
            // InternalMyDsl.g:78:2: (otherlv_0= 'Videogen' otherlv_1= '{' ( (lv_vids_2_0= ruleVideoSequence ) )+ otherlv_3= '}' )
            // InternalMyDsl.g:79:3: otherlv_0= 'Videogen' otherlv_1= '{' ( (lv_vids_2_0= ruleVideoSequence ) )+ otherlv_3= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getVideoGenAccess().getVideogenKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getVideoGenAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalMyDsl.g:87:3: ( (lv_vids_2_0= ruleVideoSequence ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=14 && LA1_0<=16)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:88:4: (lv_vids_2_0= ruleVideoSequence )
            	    {
            	    // InternalMyDsl.g:88:4: (lv_vids_2_0= ruleVideoSequence )
            	    // InternalMyDsl.g:89:5: lv_vids_2_0= ruleVideoSequence
            	    {

            	    					newCompositeNode(grammarAccess.getVideoGenAccess().getVidsVideoSequenceParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_vids_2_0=ruleVideoSequence();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getVideoGenRule());
            	    					}
            	    					add(
            	    						current,
            	    						"vids",
            	    						lv_vids_2_0,
            	    						"org.xtext.example.mydsl.MyDsl.VideoSequence");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            otherlv_3=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getVideoGenAccess().getRightCurlyBracketKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVideoGen"


    // $ANTLR start "entryRuleVideoSequence"
    // InternalMyDsl.g:114:1: entryRuleVideoSequence returns [EObject current=null] : iv_ruleVideoSequence= ruleVideoSequence EOF ;
    public final EObject entryRuleVideoSequence() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVideoSequence = null;


        try {
            // InternalMyDsl.g:114:54: (iv_ruleVideoSequence= ruleVideoSequence EOF )
            // InternalMyDsl.g:115:2: iv_ruleVideoSequence= ruleVideoSequence EOF
            {
             newCompositeNode(grammarAccess.getVideoSequenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVideoSequence=ruleVideoSequence();

            state._fsp--;

             current =iv_ruleVideoSequence; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVideoSequence"


    // $ANTLR start "ruleVideoSequence"
    // InternalMyDsl.g:121:1: ruleVideoSequence returns [EObject current=null] : (this_mandatory_0= rulemandatory | this_optional_1= ruleoptional | this_alternatives_2= rulealternatives ) ;
    public final EObject ruleVideoSequence() throws RecognitionException {
        EObject current = null;

        EObject this_mandatory_0 = null;

        EObject this_optional_1 = null;

        EObject this_alternatives_2 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:127:2: ( (this_mandatory_0= rulemandatory | this_optional_1= ruleoptional | this_alternatives_2= rulealternatives ) )
            // InternalMyDsl.g:128:2: (this_mandatory_0= rulemandatory | this_optional_1= ruleoptional | this_alternatives_2= rulealternatives )
            {
            // InternalMyDsl.g:128:2: (this_mandatory_0= rulemandatory | this_optional_1= ruleoptional | this_alternatives_2= rulealternatives )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt2=1;
                }
                break;
            case 15:
                {
                alt2=2;
                }
                break;
            case 16:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:129:3: this_mandatory_0= rulemandatory
                    {

                    			newCompositeNode(grammarAccess.getVideoSequenceAccess().getMandatoryParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_mandatory_0=rulemandatory();

                    state._fsp--;


                    			current = this_mandatory_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:138:3: this_optional_1= ruleoptional
                    {

                    			newCompositeNode(grammarAccess.getVideoSequenceAccess().getOptionalParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_optional_1=ruleoptional();

                    state._fsp--;


                    			current = this_optional_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:147:3: this_alternatives_2= rulealternatives
                    {

                    			newCompositeNode(grammarAccess.getVideoSequenceAccess().getAlternativesParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_alternatives_2=rulealternatives();

                    state._fsp--;


                    			current = this_alternatives_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVideoSequence"


    // $ANTLR start "entryRulemandatory"
    // InternalMyDsl.g:159:1: entryRulemandatory returns [EObject current=null] : iv_rulemandatory= rulemandatory EOF ;
    public final EObject entryRulemandatory() throws RecognitionException {
        EObject current = null;

        EObject iv_rulemandatory = null;


        try {
            // InternalMyDsl.g:159:50: (iv_rulemandatory= rulemandatory EOF )
            // InternalMyDsl.g:160:2: iv_rulemandatory= rulemandatory EOF
            {
             newCompositeNode(grammarAccess.getMandatoryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulemandatory=rulemandatory();

            state._fsp--;

             current =iv_rulemandatory; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulemandatory"


    // $ANTLR start "rulemandatory"
    // InternalMyDsl.g:166:1: rulemandatory returns [EObject current=null] : (otherlv_0= 'mandatory' ( (lv_vid_1_0= rulevideoseq ) ) ) ;
    public final EObject rulemandatory() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_vid_1_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:172:2: ( (otherlv_0= 'mandatory' ( (lv_vid_1_0= rulevideoseq ) ) ) )
            // InternalMyDsl.g:173:2: (otherlv_0= 'mandatory' ( (lv_vid_1_0= rulevideoseq ) ) )
            {
            // InternalMyDsl.g:173:2: (otherlv_0= 'mandatory' ( (lv_vid_1_0= rulevideoseq ) ) )
            // InternalMyDsl.g:174:3: otherlv_0= 'mandatory' ( (lv_vid_1_0= rulevideoseq ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getMandatoryAccess().getMandatoryKeyword_0());
            		
            // InternalMyDsl.g:178:3: ( (lv_vid_1_0= rulevideoseq ) )
            // InternalMyDsl.g:179:4: (lv_vid_1_0= rulevideoseq )
            {
            // InternalMyDsl.g:179:4: (lv_vid_1_0= rulevideoseq )
            // InternalMyDsl.g:180:5: lv_vid_1_0= rulevideoseq
            {

            					newCompositeNode(grammarAccess.getMandatoryAccess().getVidVideoseqParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_vid_1_0=rulevideoseq();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMandatoryRule());
            					}
            					set(
            						current,
            						"vid",
            						lv_vid_1_0,
            						"org.xtext.example.mydsl.MyDsl.videoseq");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulemandatory"


    // $ANTLR start "entryRuleoptional"
    // InternalMyDsl.g:201:1: entryRuleoptional returns [EObject current=null] : iv_ruleoptional= ruleoptional EOF ;
    public final EObject entryRuleoptional() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleoptional = null;


        try {
            // InternalMyDsl.g:201:49: (iv_ruleoptional= ruleoptional EOF )
            // InternalMyDsl.g:202:2: iv_ruleoptional= ruleoptional EOF
            {
             newCompositeNode(grammarAccess.getOptionalRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleoptional=ruleoptional();

            state._fsp--;

             current =iv_ruleoptional; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleoptional"


    // $ANTLR start "ruleoptional"
    // InternalMyDsl.g:208:1: ruleoptional returns [EObject current=null] : (otherlv_0= 'optional' ( (lv_vid_1_0= rulevideoseq ) ) ) ;
    public final EObject ruleoptional() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_vid_1_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:214:2: ( (otherlv_0= 'optional' ( (lv_vid_1_0= rulevideoseq ) ) ) )
            // InternalMyDsl.g:215:2: (otherlv_0= 'optional' ( (lv_vid_1_0= rulevideoseq ) ) )
            {
            // InternalMyDsl.g:215:2: (otherlv_0= 'optional' ( (lv_vid_1_0= rulevideoseq ) ) )
            // InternalMyDsl.g:216:3: otherlv_0= 'optional' ( (lv_vid_1_0= rulevideoseq ) )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getOptionalAccess().getOptionalKeyword_0());
            		
            // InternalMyDsl.g:220:3: ( (lv_vid_1_0= rulevideoseq ) )
            // InternalMyDsl.g:221:4: (lv_vid_1_0= rulevideoseq )
            {
            // InternalMyDsl.g:221:4: (lv_vid_1_0= rulevideoseq )
            // InternalMyDsl.g:222:5: lv_vid_1_0= rulevideoseq
            {

            					newCompositeNode(grammarAccess.getOptionalAccess().getVidVideoseqParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_vid_1_0=rulevideoseq();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOptionalRule());
            					}
            					set(
            						current,
            						"vid",
            						lv_vid_1_0,
            						"org.xtext.example.mydsl.MyDsl.videoseq");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleoptional"


    // $ANTLR start "entryRulealternatives"
    // InternalMyDsl.g:243:1: entryRulealternatives returns [EObject current=null] : iv_rulealternatives= rulealternatives EOF ;
    public final EObject entryRulealternatives() throws RecognitionException {
        EObject current = null;

        EObject iv_rulealternatives = null;


        try {
            // InternalMyDsl.g:243:53: (iv_rulealternatives= rulealternatives EOF )
            // InternalMyDsl.g:244:2: iv_rulealternatives= rulealternatives EOF
            {
             newCompositeNode(grammarAccess.getAlternativesRule()); 
            pushFollow(FOLLOW_1);
            iv_rulealternatives=rulealternatives();

            state._fsp--;

             current =iv_rulealternatives; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulealternatives"


    // $ANTLR start "rulealternatives"
    // InternalMyDsl.g:250:1: rulealternatives returns [EObject current=null] : (otherlv_0= 'alternatives' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_vids_3_0= rulevideoseq ) )+ ) ;
    public final EObject rulealternatives() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        Token otherlv_2=null;
        EObject lv_vids_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:256:2: ( (otherlv_0= 'alternatives' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_vids_3_0= rulevideoseq ) )+ ) )
            // InternalMyDsl.g:257:2: (otherlv_0= 'alternatives' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_vids_3_0= rulevideoseq ) )+ )
            {
            // InternalMyDsl.g:257:2: (otherlv_0= 'alternatives' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_vids_3_0= rulevideoseq ) )+ )
            // InternalMyDsl.g:258:3: otherlv_0= 'alternatives' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_vids_3_0= rulevideoseq ) )+
            {
            otherlv_0=(Token)match(input,16,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getAlternativesAccess().getAlternativesKeyword_0());
            		
            // InternalMyDsl.g:262:3: ( (lv_id_1_0= RULE_ID ) )
            // InternalMyDsl.g:263:4: (lv_id_1_0= RULE_ID )
            {
            // InternalMyDsl.g:263:4: (lv_id_1_0= RULE_ID )
            // InternalMyDsl.g:264:5: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_id_1_0, grammarAccess.getAlternativesAccess().getIdIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAlternativesRule());
            					}
            					setWithLastConsumed(
            						current,
            						"id",
            						lv_id_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getAlternativesAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalMyDsl.g:284:3: ( (lv_vids_3_0= rulevideoseq ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==17) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalMyDsl.g:285:4: (lv_vids_3_0= rulevideoseq )
            	    {
            	    // InternalMyDsl.g:285:4: (lv_vids_3_0= rulevideoseq )
            	    // InternalMyDsl.g:286:5: lv_vids_3_0= rulevideoseq
            	    {

            	    					newCompositeNode(grammarAccess.getAlternativesAccess().getVidsVideoseqParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_vids_3_0=rulevideoseq();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAlternativesRule());
            	    					}
            	    					add(
            	    						current,
            	    						"vids",
            	    						lv_vids_3_0,
            	    						"org.xtext.example.mydsl.MyDsl.videoseq");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulealternatives"


    // $ANTLR start "entryRulevideoseq"
    // InternalMyDsl.g:307:1: entryRulevideoseq returns [EObject current=null] : iv_rulevideoseq= rulevideoseq EOF ;
    public final EObject entryRulevideoseq() throws RecognitionException {
        EObject current = null;

        EObject iv_rulevideoseq = null;


        try {
            // InternalMyDsl.g:307:49: (iv_rulevideoseq= rulevideoseq EOF )
            // InternalMyDsl.g:308:2: iv_rulevideoseq= rulevideoseq EOF
            {
             newCompositeNode(grammarAccess.getVideoseqRule()); 
            pushFollow(FOLLOW_1);
            iv_rulevideoseq=rulevideoseq();

            state._fsp--;

             current =iv_rulevideoseq; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulevideoseq"


    // $ANTLR start "rulevideoseq"
    // InternalMyDsl.g:314:1: rulevideoseq returns [EObject current=null] : (otherlv_0= 'videoseq' ( (lv_id_1_0= RULE_ID ) ) ( (lv_url_2_0= RULE_STRING ) ) ) ;
    public final EObject rulevideoseq() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        Token lv_url_2_0=null;


        	enterRule();

        try {
            // InternalMyDsl.g:320:2: ( (otherlv_0= 'videoseq' ( (lv_id_1_0= RULE_ID ) ) ( (lv_url_2_0= RULE_STRING ) ) ) )
            // InternalMyDsl.g:321:2: (otherlv_0= 'videoseq' ( (lv_id_1_0= RULE_ID ) ) ( (lv_url_2_0= RULE_STRING ) ) )
            {
            // InternalMyDsl.g:321:2: (otherlv_0= 'videoseq' ( (lv_id_1_0= RULE_ID ) ) ( (lv_url_2_0= RULE_STRING ) ) )
            // InternalMyDsl.g:322:3: otherlv_0= 'videoseq' ( (lv_id_1_0= RULE_ID ) ) ( (lv_url_2_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,17,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getVideoseqAccess().getVideoseqKeyword_0());
            		
            // InternalMyDsl.g:326:3: ( (lv_id_1_0= RULE_ID ) )
            // InternalMyDsl.g:327:4: (lv_id_1_0= RULE_ID )
            {
            // InternalMyDsl.g:327:4: (lv_id_1_0= RULE_ID )
            // InternalMyDsl.g:328:5: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_id_1_0, grammarAccess.getVideoseqAccess().getIdIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getVideoseqRule());
            					}
            					setWithLastConsumed(
            						current,
            						"id",
            						lv_id_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalMyDsl.g:344:3: ( (lv_url_2_0= RULE_STRING ) )
            // InternalMyDsl.g:345:4: (lv_url_2_0= RULE_STRING )
            {
            // InternalMyDsl.g:345:4: (lv_url_2_0= RULE_STRING )
            // InternalMyDsl.g:346:5: lv_url_2_0= RULE_STRING
            {
            lv_url_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_url_2_0, grammarAccess.getVideoseqAccess().getUrlSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getVideoseqRule());
            					}
            					setWithLastConsumed(
            						current,
            						"url",
            						lv_url_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulevideoseq"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x000000000001C000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000001E000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000020L});

}