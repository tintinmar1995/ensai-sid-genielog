/**
 * generated by Xtext 2.16.0
 */
package org.xtext.example.mydsl.videogen.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.xtext.example.mydsl.videogen.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VideogenFactoryImpl extends EFactoryImpl implements VideogenFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static VideogenFactory init()
  {
    try
    {
      VideogenFactory theVideogenFactory = (VideogenFactory)EPackage.Registry.INSTANCE.getEFactory(VideogenPackage.eNS_URI);
      if (theVideogenFactory != null)
      {
        return theVideogenFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new VideogenFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VideogenFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case VideogenPackage.VIDEO_GEN: return createVideoGen();
      case VideogenPackage.VIDEO_SEQUENCE: return createVideoSequence();
      case VideogenPackage.MANDATORY: return createmandatory();
      case VideogenPackage.OPTIONAL: return createoptional();
      case VideogenPackage.ALTERNATIVES: return createalternatives();
      case VideogenPackage.VIDEOSEQ: return createvideoseq();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public VideoGen createVideoGen()
  {
    VideoGenImpl videoGen = new VideoGenImpl();
    return videoGen;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public VideoSequence createVideoSequence()
  {
    VideoSequenceImpl videoSequence = new VideoSequenceImpl();
    return videoSequence;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public mandatory createmandatory()
  {
    mandatoryImpl mandatory = new mandatoryImpl();
    return mandatory;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public optional createoptional()
  {
    optionalImpl optional = new optionalImpl();
    return optional;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public alternatives createalternatives()
  {
    alternativesImpl alternatives = new alternativesImpl();
    return alternatives;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public videoseq createvideoseq()
  {
    videoseqImpl videoseq = new videoseqImpl();
    return videoseq;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public VideogenPackage getVideogenPackage()
  {
    return (VideogenPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static VideogenPackage getPackage()
  {
    return VideogenPackage.eINSTANCE;
  }

} //VideogenFactoryImpl
