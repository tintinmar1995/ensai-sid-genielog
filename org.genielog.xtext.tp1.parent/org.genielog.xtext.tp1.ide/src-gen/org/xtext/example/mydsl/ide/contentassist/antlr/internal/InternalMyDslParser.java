package org.xtext.example.mydsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Videogen'", "'{'", "'}'", "'mandatory'", "'optional'", "'alternatives'", "'videoseq'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int RULE_INT=6;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }


    	private MyDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(MyDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleVideoGen"
    // InternalMyDsl.g:53:1: entryRuleVideoGen : ruleVideoGen EOF ;
    public final void entryRuleVideoGen() throws RecognitionException {
        try {
            // InternalMyDsl.g:54:1: ( ruleVideoGen EOF )
            // InternalMyDsl.g:55:1: ruleVideoGen EOF
            {
             before(grammarAccess.getVideoGenRule()); 
            pushFollow(FOLLOW_1);
            ruleVideoGen();

            state._fsp--;

             after(grammarAccess.getVideoGenRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVideoGen"


    // $ANTLR start "ruleVideoGen"
    // InternalMyDsl.g:62:1: ruleVideoGen : ( ( rule__VideoGen__Group__0 ) ) ;
    public final void ruleVideoGen() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:66:2: ( ( ( rule__VideoGen__Group__0 ) ) )
            // InternalMyDsl.g:67:2: ( ( rule__VideoGen__Group__0 ) )
            {
            // InternalMyDsl.g:67:2: ( ( rule__VideoGen__Group__0 ) )
            // InternalMyDsl.g:68:3: ( rule__VideoGen__Group__0 )
            {
             before(grammarAccess.getVideoGenAccess().getGroup()); 
            // InternalMyDsl.g:69:3: ( rule__VideoGen__Group__0 )
            // InternalMyDsl.g:69:4: rule__VideoGen__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__VideoGen__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVideoGenAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVideoGen"


    // $ANTLR start "entryRuleVideoSequence"
    // InternalMyDsl.g:78:1: entryRuleVideoSequence : ruleVideoSequence EOF ;
    public final void entryRuleVideoSequence() throws RecognitionException {
        try {
            // InternalMyDsl.g:79:1: ( ruleVideoSequence EOF )
            // InternalMyDsl.g:80:1: ruleVideoSequence EOF
            {
             before(grammarAccess.getVideoSequenceRule()); 
            pushFollow(FOLLOW_1);
            ruleVideoSequence();

            state._fsp--;

             after(grammarAccess.getVideoSequenceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVideoSequence"


    // $ANTLR start "ruleVideoSequence"
    // InternalMyDsl.g:87:1: ruleVideoSequence : ( ( rule__VideoSequence__Alternatives ) ) ;
    public final void ruleVideoSequence() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:91:2: ( ( ( rule__VideoSequence__Alternatives ) ) )
            // InternalMyDsl.g:92:2: ( ( rule__VideoSequence__Alternatives ) )
            {
            // InternalMyDsl.g:92:2: ( ( rule__VideoSequence__Alternatives ) )
            // InternalMyDsl.g:93:3: ( rule__VideoSequence__Alternatives )
            {
             before(grammarAccess.getVideoSequenceAccess().getAlternatives()); 
            // InternalMyDsl.g:94:3: ( rule__VideoSequence__Alternatives )
            // InternalMyDsl.g:94:4: rule__VideoSequence__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__VideoSequence__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getVideoSequenceAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVideoSequence"


    // $ANTLR start "entryRulemandatory"
    // InternalMyDsl.g:103:1: entryRulemandatory : rulemandatory EOF ;
    public final void entryRulemandatory() throws RecognitionException {
        try {
            // InternalMyDsl.g:104:1: ( rulemandatory EOF )
            // InternalMyDsl.g:105:1: rulemandatory EOF
            {
             before(grammarAccess.getMandatoryRule()); 
            pushFollow(FOLLOW_1);
            rulemandatory();

            state._fsp--;

             after(grammarAccess.getMandatoryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulemandatory"


    // $ANTLR start "rulemandatory"
    // InternalMyDsl.g:112:1: rulemandatory : ( ( rule__Mandatory__Group__0 ) ) ;
    public final void rulemandatory() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:116:2: ( ( ( rule__Mandatory__Group__0 ) ) )
            // InternalMyDsl.g:117:2: ( ( rule__Mandatory__Group__0 ) )
            {
            // InternalMyDsl.g:117:2: ( ( rule__Mandatory__Group__0 ) )
            // InternalMyDsl.g:118:3: ( rule__Mandatory__Group__0 )
            {
             before(grammarAccess.getMandatoryAccess().getGroup()); 
            // InternalMyDsl.g:119:3: ( rule__Mandatory__Group__0 )
            // InternalMyDsl.g:119:4: rule__Mandatory__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Mandatory__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMandatoryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulemandatory"


    // $ANTLR start "entryRuleoptional"
    // InternalMyDsl.g:128:1: entryRuleoptional : ruleoptional EOF ;
    public final void entryRuleoptional() throws RecognitionException {
        try {
            // InternalMyDsl.g:129:1: ( ruleoptional EOF )
            // InternalMyDsl.g:130:1: ruleoptional EOF
            {
             before(grammarAccess.getOptionalRule()); 
            pushFollow(FOLLOW_1);
            ruleoptional();

            state._fsp--;

             after(grammarAccess.getOptionalRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleoptional"


    // $ANTLR start "ruleoptional"
    // InternalMyDsl.g:137:1: ruleoptional : ( ( rule__Optional__Group__0 ) ) ;
    public final void ruleoptional() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:141:2: ( ( ( rule__Optional__Group__0 ) ) )
            // InternalMyDsl.g:142:2: ( ( rule__Optional__Group__0 ) )
            {
            // InternalMyDsl.g:142:2: ( ( rule__Optional__Group__0 ) )
            // InternalMyDsl.g:143:3: ( rule__Optional__Group__0 )
            {
             before(grammarAccess.getOptionalAccess().getGroup()); 
            // InternalMyDsl.g:144:3: ( rule__Optional__Group__0 )
            // InternalMyDsl.g:144:4: rule__Optional__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Optional__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOptionalAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleoptional"


    // $ANTLR start "entryRulealternatives"
    // InternalMyDsl.g:153:1: entryRulealternatives : rulealternatives EOF ;
    public final void entryRulealternatives() throws RecognitionException {
        try {
            // InternalMyDsl.g:154:1: ( rulealternatives EOF )
            // InternalMyDsl.g:155:1: rulealternatives EOF
            {
             before(grammarAccess.getAlternativesRule()); 
            pushFollow(FOLLOW_1);
            rulealternatives();

            state._fsp--;

             after(grammarAccess.getAlternativesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulealternatives"


    // $ANTLR start "rulealternatives"
    // InternalMyDsl.g:162:1: rulealternatives : ( ( rule__Alternatives__Group__0 ) ) ;
    public final void rulealternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:166:2: ( ( ( rule__Alternatives__Group__0 ) ) )
            // InternalMyDsl.g:167:2: ( ( rule__Alternatives__Group__0 ) )
            {
            // InternalMyDsl.g:167:2: ( ( rule__Alternatives__Group__0 ) )
            // InternalMyDsl.g:168:3: ( rule__Alternatives__Group__0 )
            {
             before(grammarAccess.getAlternativesAccess().getGroup()); 
            // InternalMyDsl.g:169:3: ( rule__Alternatives__Group__0 )
            // InternalMyDsl.g:169:4: rule__Alternatives__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Alternatives__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAlternativesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulealternatives"


    // $ANTLR start "entryRulevideoseq"
    // InternalMyDsl.g:178:1: entryRulevideoseq : rulevideoseq EOF ;
    public final void entryRulevideoseq() throws RecognitionException {
        try {
            // InternalMyDsl.g:179:1: ( rulevideoseq EOF )
            // InternalMyDsl.g:180:1: rulevideoseq EOF
            {
             before(grammarAccess.getVideoseqRule()); 
            pushFollow(FOLLOW_1);
            rulevideoseq();

            state._fsp--;

             after(grammarAccess.getVideoseqRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulevideoseq"


    // $ANTLR start "rulevideoseq"
    // InternalMyDsl.g:187:1: rulevideoseq : ( ( rule__Videoseq__Group__0 ) ) ;
    public final void rulevideoseq() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:191:2: ( ( ( rule__Videoseq__Group__0 ) ) )
            // InternalMyDsl.g:192:2: ( ( rule__Videoseq__Group__0 ) )
            {
            // InternalMyDsl.g:192:2: ( ( rule__Videoseq__Group__0 ) )
            // InternalMyDsl.g:193:3: ( rule__Videoseq__Group__0 )
            {
             before(grammarAccess.getVideoseqAccess().getGroup()); 
            // InternalMyDsl.g:194:3: ( rule__Videoseq__Group__0 )
            // InternalMyDsl.g:194:4: rule__Videoseq__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Videoseq__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVideoseqAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulevideoseq"


    // $ANTLR start "rule__VideoSequence__Alternatives"
    // InternalMyDsl.g:202:1: rule__VideoSequence__Alternatives : ( ( rulemandatory ) | ( ruleoptional ) | ( rulealternatives ) );
    public final void rule__VideoSequence__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:206:1: ( ( rulemandatory ) | ( ruleoptional ) | ( rulealternatives ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt1=1;
                }
                break;
            case 15:
                {
                alt1=2;
                }
                break;
            case 16:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalMyDsl.g:207:2: ( rulemandatory )
                    {
                    // InternalMyDsl.g:207:2: ( rulemandatory )
                    // InternalMyDsl.g:208:3: rulemandatory
                    {
                     before(grammarAccess.getVideoSequenceAccess().getMandatoryParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    rulemandatory();

                    state._fsp--;

                     after(grammarAccess.getVideoSequenceAccess().getMandatoryParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:213:2: ( ruleoptional )
                    {
                    // InternalMyDsl.g:213:2: ( ruleoptional )
                    // InternalMyDsl.g:214:3: ruleoptional
                    {
                     before(grammarAccess.getVideoSequenceAccess().getOptionalParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleoptional();

                    state._fsp--;

                     after(grammarAccess.getVideoSequenceAccess().getOptionalParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:219:2: ( rulealternatives )
                    {
                    // InternalMyDsl.g:219:2: ( rulealternatives )
                    // InternalMyDsl.g:220:3: rulealternatives
                    {
                     before(grammarAccess.getVideoSequenceAccess().getAlternativesParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    rulealternatives();

                    state._fsp--;

                     after(grammarAccess.getVideoSequenceAccess().getAlternativesParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Alternatives"


    // $ANTLR start "rule__VideoGen__Group__0"
    // InternalMyDsl.g:229:1: rule__VideoGen__Group__0 : rule__VideoGen__Group__0__Impl rule__VideoGen__Group__1 ;
    public final void rule__VideoGen__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:233:1: ( rule__VideoGen__Group__0__Impl rule__VideoGen__Group__1 )
            // InternalMyDsl.g:234:2: rule__VideoGen__Group__0__Impl rule__VideoGen__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__VideoGen__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VideoGen__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__0"


    // $ANTLR start "rule__VideoGen__Group__0__Impl"
    // InternalMyDsl.g:241:1: rule__VideoGen__Group__0__Impl : ( 'Videogen' ) ;
    public final void rule__VideoGen__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:245:1: ( ( 'Videogen' ) )
            // InternalMyDsl.g:246:1: ( 'Videogen' )
            {
            // InternalMyDsl.g:246:1: ( 'Videogen' )
            // InternalMyDsl.g:247:2: 'Videogen'
            {
             before(grammarAccess.getVideoGenAccess().getVideogenKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getVideoGenAccess().getVideogenKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__0__Impl"


    // $ANTLR start "rule__VideoGen__Group__1"
    // InternalMyDsl.g:256:1: rule__VideoGen__Group__1 : rule__VideoGen__Group__1__Impl rule__VideoGen__Group__2 ;
    public final void rule__VideoGen__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:260:1: ( rule__VideoGen__Group__1__Impl rule__VideoGen__Group__2 )
            // InternalMyDsl.g:261:2: rule__VideoGen__Group__1__Impl rule__VideoGen__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__VideoGen__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VideoGen__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__1"


    // $ANTLR start "rule__VideoGen__Group__1__Impl"
    // InternalMyDsl.g:268:1: rule__VideoGen__Group__1__Impl : ( '{' ) ;
    public final void rule__VideoGen__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:272:1: ( ( '{' ) )
            // InternalMyDsl.g:273:1: ( '{' )
            {
            // InternalMyDsl.g:273:1: ( '{' )
            // InternalMyDsl.g:274:2: '{'
            {
             before(grammarAccess.getVideoGenAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getVideoGenAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__1__Impl"


    // $ANTLR start "rule__VideoGen__Group__2"
    // InternalMyDsl.g:283:1: rule__VideoGen__Group__2 : rule__VideoGen__Group__2__Impl rule__VideoGen__Group__3 ;
    public final void rule__VideoGen__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:287:1: ( rule__VideoGen__Group__2__Impl rule__VideoGen__Group__3 )
            // InternalMyDsl.g:288:2: rule__VideoGen__Group__2__Impl rule__VideoGen__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__VideoGen__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VideoGen__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__2"


    // $ANTLR start "rule__VideoGen__Group__2__Impl"
    // InternalMyDsl.g:295:1: rule__VideoGen__Group__2__Impl : ( ( ( rule__VideoGen__VidsAssignment_2 ) ) ( ( rule__VideoGen__VidsAssignment_2 )* ) ) ;
    public final void rule__VideoGen__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:299:1: ( ( ( ( rule__VideoGen__VidsAssignment_2 ) ) ( ( rule__VideoGen__VidsAssignment_2 )* ) ) )
            // InternalMyDsl.g:300:1: ( ( ( rule__VideoGen__VidsAssignment_2 ) ) ( ( rule__VideoGen__VidsAssignment_2 )* ) )
            {
            // InternalMyDsl.g:300:1: ( ( ( rule__VideoGen__VidsAssignment_2 ) ) ( ( rule__VideoGen__VidsAssignment_2 )* ) )
            // InternalMyDsl.g:301:2: ( ( rule__VideoGen__VidsAssignment_2 ) ) ( ( rule__VideoGen__VidsAssignment_2 )* )
            {
            // InternalMyDsl.g:301:2: ( ( rule__VideoGen__VidsAssignment_2 ) )
            // InternalMyDsl.g:302:3: ( rule__VideoGen__VidsAssignment_2 )
            {
             before(grammarAccess.getVideoGenAccess().getVidsAssignment_2()); 
            // InternalMyDsl.g:303:3: ( rule__VideoGen__VidsAssignment_2 )
            // InternalMyDsl.g:303:4: rule__VideoGen__VidsAssignment_2
            {
            pushFollow(FOLLOW_6);
            rule__VideoGen__VidsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getVideoGenAccess().getVidsAssignment_2()); 

            }

            // InternalMyDsl.g:306:2: ( ( rule__VideoGen__VidsAssignment_2 )* )
            // InternalMyDsl.g:307:3: ( rule__VideoGen__VidsAssignment_2 )*
            {
             before(grammarAccess.getVideoGenAccess().getVidsAssignment_2()); 
            // InternalMyDsl.g:308:3: ( rule__VideoGen__VidsAssignment_2 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=14 && LA2_0<=16)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalMyDsl.g:308:4: rule__VideoGen__VidsAssignment_2
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__VideoGen__VidsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getVideoGenAccess().getVidsAssignment_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__2__Impl"


    // $ANTLR start "rule__VideoGen__Group__3"
    // InternalMyDsl.g:317:1: rule__VideoGen__Group__3 : rule__VideoGen__Group__3__Impl ;
    public final void rule__VideoGen__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:321:1: ( rule__VideoGen__Group__3__Impl )
            // InternalMyDsl.g:322:2: rule__VideoGen__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__VideoGen__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__3"


    // $ANTLR start "rule__VideoGen__Group__3__Impl"
    // InternalMyDsl.g:328:1: rule__VideoGen__Group__3__Impl : ( '}' ) ;
    public final void rule__VideoGen__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:332:1: ( ( '}' ) )
            // InternalMyDsl.g:333:1: ( '}' )
            {
            // InternalMyDsl.g:333:1: ( '}' )
            // InternalMyDsl.g:334:2: '}'
            {
             before(grammarAccess.getVideoGenAccess().getRightCurlyBracketKeyword_3()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getVideoGenAccess().getRightCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__3__Impl"


    // $ANTLR start "rule__Mandatory__Group__0"
    // InternalMyDsl.g:344:1: rule__Mandatory__Group__0 : rule__Mandatory__Group__0__Impl rule__Mandatory__Group__1 ;
    public final void rule__Mandatory__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:348:1: ( rule__Mandatory__Group__0__Impl rule__Mandatory__Group__1 )
            // InternalMyDsl.g:349:2: rule__Mandatory__Group__0__Impl rule__Mandatory__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Mandatory__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mandatory__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__Group__0"


    // $ANTLR start "rule__Mandatory__Group__0__Impl"
    // InternalMyDsl.g:356:1: rule__Mandatory__Group__0__Impl : ( 'mandatory' ) ;
    public final void rule__Mandatory__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:360:1: ( ( 'mandatory' ) )
            // InternalMyDsl.g:361:1: ( 'mandatory' )
            {
            // InternalMyDsl.g:361:1: ( 'mandatory' )
            // InternalMyDsl.g:362:2: 'mandatory'
            {
             before(grammarAccess.getMandatoryAccess().getMandatoryKeyword_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getMandatoryAccess().getMandatoryKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__Group__0__Impl"


    // $ANTLR start "rule__Mandatory__Group__1"
    // InternalMyDsl.g:371:1: rule__Mandatory__Group__1 : rule__Mandatory__Group__1__Impl ;
    public final void rule__Mandatory__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:375:1: ( rule__Mandatory__Group__1__Impl )
            // InternalMyDsl.g:376:2: rule__Mandatory__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mandatory__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__Group__1"


    // $ANTLR start "rule__Mandatory__Group__1__Impl"
    // InternalMyDsl.g:382:1: rule__Mandatory__Group__1__Impl : ( ( rule__Mandatory__VidAssignment_1 ) ) ;
    public final void rule__Mandatory__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:386:1: ( ( ( rule__Mandatory__VidAssignment_1 ) ) )
            // InternalMyDsl.g:387:1: ( ( rule__Mandatory__VidAssignment_1 ) )
            {
            // InternalMyDsl.g:387:1: ( ( rule__Mandatory__VidAssignment_1 ) )
            // InternalMyDsl.g:388:2: ( rule__Mandatory__VidAssignment_1 )
            {
             before(grammarAccess.getMandatoryAccess().getVidAssignment_1()); 
            // InternalMyDsl.g:389:2: ( rule__Mandatory__VidAssignment_1 )
            // InternalMyDsl.g:389:3: rule__Mandatory__VidAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Mandatory__VidAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMandatoryAccess().getVidAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__Group__1__Impl"


    // $ANTLR start "rule__Optional__Group__0"
    // InternalMyDsl.g:398:1: rule__Optional__Group__0 : rule__Optional__Group__0__Impl rule__Optional__Group__1 ;
    public final void rule__Optional__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:402:1: ( rule__Optional__Group__0__Impl rule__Optional__Group__1 )
            // InternalMyDsl.g:403:2: rule__Optional__Group__0__Impl rule__Optional__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Optional__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Optional__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__Group__0"


    // $ANTLR start "rule__Optional__Group__0__Impl"
    // InternalMyDsl.g:410:1: rule__Optional__Group__0__Impl : ( 'optional' ) ;
    public final void rule__Optional__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:414:1: ( ( 'optional' ) )
            // InternalMyDsl.g:415:1: ( 'optional' )
            {
            // InternalMyDsl.g:415:1: ( 'optional' )
            // InternalMyDsl.g:416:2: 'optional'
            {
             before(grammarAccess.getOptionalAccess().getOptionalKeyword_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getOptionalAccess().getOptionalKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__Group__0__Impl"


    // $ANTLR start "rule__Optional__Group__1"
    // InternalMyDsl.g:425:1: rule__Optional__Group__1 : rule__Optional__Group__1__Impl ;
    public final void rule__Optional__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:429:1: ( rule__Optional__Group__1__Impl )
            // InternalMyDsl.g:430:2: rule__Optional__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Optional__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__Group__1"


    // $ANTLR start "rule__Optional__Group__1__Impl"
    // InternalMyDsl.g:436:1: rule__Optional__Group__1__Impl : ( ( rule__Optional__VidAssignment_1 ) ) ;
    public final void rule__Optional__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:440:1: ( ( ( rule__Optional__VidAssignment_1 ) ) )
            // InternalMyDsl.g:441:1: ( ( rule__Optional__VidAssignment_1 ) )
            {
            // InternalMyDsl.g:441:1: ( ( rule__Optional__VidAssignment_1 ) )
            // InternalMyDsl.g:442:2: ( rule__Optional__VidAssignment_1 )
            {
             before(grammarAccess.getOptionalAccess().getVidAssignment_1()); 
            // InternalMyDsl.g:443:2: ( rule__Optional__VidAssignment_1 )
            // InternalMyDsl.g:443:3: rule__Optional__VidAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Optional__VidAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOptionalAccess().getVidAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__Group__1__Impl"


    // $ANTLR start "rule__Alternatives__Group__0"
    // InternalMyDsl.g:452:1: rule__Alternatives__Group__0 : rule__Alternatives__Group__0__Impl rule__Alternatives__Group__1 ;
    public final void rule__Alternatives__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:456:1: ( rule__Alternatives__Group__0__Impl rule__Alternatives__Group__1 )
            // InternalMyDsl.g:457:2: rule__Alternatives__Group__0__Impl rule__Alternatives__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Alternatives__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Alternatives__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__0"


    // $ANTLR start "rule__Alternatives__Group__0__Impl"
    // InternalMyDsl.g:464:1: rule__Alternatives__Group__0__Impl : ( 'alternatives' ) ;
    public final void rule__Alternatives__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:468:1: ( ( 'alternatives' ) )
            // InternalMyDsl.g:469:1: ( 'alternatives' )
            {
            // InternalMyDsl.g:469:1: ( 'alternatives' )
            // InternalMyDsl.g:470:2: 'alternatives'
            {
             before(grammarAccess.getAlternativesAccess().getAlternativesKeyword_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getAlternativesAccess().getAlternativesKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__0__Impl"


    // $ANTLR start "rule__Alternatives__Group__1"
    // InternalMyDsl.g:479:1: rule__Alternatives__Group__1 : rule__Alternatives__Group__1__Impl rule__Alternatives__Group__2 ;
    public final void rule__Alternatives__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:483:1: ( rule__Alternatives__Group__1__Impl rule__Alternatives__Group__2 )
            // InternalMyDsl.g:484:2: rule__Alternatives__Group__1__Impl rule__Alternatives__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Alternatives__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Alternatives__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__1"


    // $ANTLR start "rule__Alternatives__Group__1__Impl"
    // InternalMyDsl.g:491:1: rule__Alternatives__Group__1__Impl : ( ( rule__Alternatives__IdAssignment_1 ) ) ;
    public final void rule__Alternatives__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:495:1: ( ( ( rule__Alternatives__IdAssignment_1 ) ) )
            // InternalMyDsl.g:496:1: ( ( rule__Alternatives__IdAssignment_1 ) )
            {
            // InternalMyDsl.g:496:1: ( ( rule__Alternatives__IdAssignment_1 ) )
            // InternalMyDsl.g:497:2: ( rule__Alternatives__IdAssignment_1 )
            {
             before(grammarAccess.getAlternativesAccess().getIdAssignment_1()); 
            // InternalMyDsl.g:498:2: ( rule__Alternatives__IdAssignment_1 )
            // InternalMyDsl.g:498:3: rule__Alternatives__IdAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Alternatives__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAlternativesAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__1__Impl"


    // $ANTLR start "rule__Alternatives__Group__2"
    // InternalMyDsl.g:506:1: rule__Alternatives__Group__2 : rule__Alternatives__Group__2__Impl rule__Alternatives__Group__3 ;
    public final void rule__Alternatives__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:510:1: ( rule__Alternatives__Group__2__Impl rule__Alternatives__Group__3 )
            // InternalMyDsl.g:511:2: rule__Alternatives__Group__2__Impl rule__Alternatives__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Alternatives__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Alternatives__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__2"


    // $ANTLR start "rule__Alternatives__Group__2__Impl"
    // InternalMyDsl.g:518:1: rule__Alternatives__Group__2__Impl : ( '{' ) ;
    public final void rule__Alternatives__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:522:1: ( ( '{' ) )
            // InternalMyDsl.g:523:1: ( '{' )
            {
            // InternalMyDsl.g:523:1: ( '{' )
            // InternalMyDsl.g:524:2: '{'
            {
             before(grammarAccess.getAlternativesAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getAlternativesAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__2__Impl"


    // $ANTLR start "rule__Alternatives__Group__3"
    // InternalMyDsl.g:533:1: rule__Alternatives__Group__3 : rule__Alternatives__Group__3__Impl ;
    public final void rule__Alternatives__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:537:1: ( rule__Alternatives__Group__3__Impl )
            // InternalMyDsl.g:538:2: rule__Alternatives__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Alternatives__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__3"


    // $ANTLR start "rule__Alternatives__Group__3__Impl"
    // InternalMyDsl.g:544:1: rule__Alternatives__Group__3__Impl : ( ( ( rule__Alternatives__VidsAssignment_3 ) ) ( ( rule__Alternatives__VidsAssignment_3 )* ) ) ;
    public final void rule__Alternatives__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:548:1: ( ( ( ( rule__Alternatives__VidsAssignment_3 ) ) ( ( rule__Alternatives__VidsAssignment_3 )* ) ) )
            // InternalMyDsl.g:549:1: ( ( ( rule__Alternatives__VidsAssignment_3 ) ) ( ( rule__Alternatives__VidsAssignment_3 )* ) )
            {
            // InternalMyDsl.g:549:1: ( ( ( rule__Alternatives__VidsAssignment_3 ) ) ( ( rule__Alternatives__VidsAssignment_3 )* ) )
            // InternalMyDsl.g:550:2: ( ( rule__Alternatives__VidsAssignment_3 ) ) ( ( rule__Alternatives__VidsAssignment_3 )* )
            {
            // InternalMyDsl.g:550:2: ( ( rule__Alternatives__VidsAssignment_3 ) )
            // InternalMyDsl.g:551:3: ( rule__Alternatives__VidsAssignment_3 )
            {
             before(grammarAccess.getAlternativesAccess().getVidsAssignment_3()); 
            // InternalMyDsl.g:552:3: ( rule__Alternatives__VidsAssignment_3 )
            // InternalMyDsl.g:552:4: rule__Alternatives__VidsAssignment_3
            {
            pushFollow(FOLLOW_9);
            rule__Alternatives__VidsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAlternativesAccess().getVidsAssignment_3()); 

            }

            // InternalMyDsl.g:555:2: ( ( rule__Alternatives__VidsAssignment_3 )* )
            // InternalMyDsl.g:556:3: ( rule__Alternatives__VidsAssignment_3 )*
            {
             before(grammarAccess.getAlternativesAccess().getVidsAssignment_3()); 
            // InternalMyDsl.g:557:3: ( rule__Alternatives__VidsAssignment_3 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==17) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalMyDsl.g:557:4: rule__Alternatives__VidsAssignment_3
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Alternatives__VidsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getAlternativesAccess().getVidsAssignment_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__3__Impl"


    // $ANTLR start "rule__Videoseq__Group__0"
    // InternalMyDsl.g:567:1: rule__Videoseq__Group__0 : rule__Videoseq__Group__0__Impl rule__Videoseq__Group__1 ;
    public final void rule__Videoseq__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:571:1: ( rule__Videoseq__Group__0__Impl rule__Videoseq__Group__1 )
            // InternalMyDsl.g:572:2: rule__Videoseq__Group__0__Impl rule__Videoseq__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Videoseq__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Videoseq__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Videoseq__Group__0"


    // $ANTLR start "rule__Videoseq__Group__0__Impl"
    // InternalMyDsl.g:579:1: rule__Videoseq__Group__0__Impl : ( 'videoseq' ) ;
    public final void rule__Videoseq__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:583:1: ( ( 'videoseq' ) )
            // InternalMyDsl.g:584:1: ( 'videoseq' )
            {
            // InternalMyDsl.g:584:1: ( 'videoseq' )
            // InternalMyDsl.g:585:2: 'videoseq'
            {
             before(grammarAccess.getVideoseqAccess().getVideoseqKeyword_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getVideoseqAccess().getVideoseqKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Videoseq__Group__0__Impl"


    // $ANTLR start "rule__Videoseq__Group__1"
    // InternalMyDsl.g:594:1: rule__Videoseq__Group__1 : rule__Videoseq__Group__1__Impl rule__Videoseq__Group__2 ;
    public final void rule__Videoseq__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:598:1: ( rule__Videoseq__Group__1__Impl rule__Videoseq__Group__2 )
            // InternalMyDsl.g:599:2: rule__Videoseq__Group__1__Impl rule__Videoseq__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__Videoseq__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Videoseq__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Videoseq__Group__1"


    // $ANTLR start "rule__Videoseq__Group__1__Impl"
    // InternalMyDsl.g:606:1: rule__Videoseq__Group__1__Impl : ( ( rule__Videoseq__IdAssignment_1 ) ) ;
    public final void rule__Videoseq__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:610:1: ( ( ( rule__Videoseq__IdAssignment_1 ) ) )
            // InternalMyDsl.g:611:1: ( ( rule__Videoseq__IdAssignment_1 ) )
            {
            // InternalMyDsl.g:611:1: ( ( rule__Videoseq__IdAssignment_1 ) )
            // InternalMyDsl.g:612:2: ( rule__Videoseq__IdAssignment_1 )
            {
             before(grammarAccess.getVideoseqAccess().getIdAssignment_1()); 
            // InternalMyDsl.g:613:2: ( rule__Videoseq__IdAssignment_1 )
            // InternalMyDsl.g:613:3: rule__Videoseq__IdAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Videoseq__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getVideoseqAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Videoseq__Group__1__Impl"


    // $ANTLR start "rule__Videoseq__Group__2"
    // InternalMyDsl.g:621:1: rule__Videoseq__Group__2 : rule__Videoseq__Group__2__Impl ;
    public final void rule__Videoseq__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:625:1: ( rule__Videoseq__Group__2__Impl )
            // InternalMyDsl.g:626:2: rule__Videoseq__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Videoseq__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Videoseq__Group__2"


    // $ANTLR start "rule__Videoseq__Group__2__Impl"
    // InternalMyDsl.g:632:1: rule__Videoseq__Group__2__Impl : ( ( rule__Videoseq__UrlAssignment_2 ) ) ;
    public final void rule__Videoseq__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:636:1: ( ( ( rule__Videoseq__UrlAssignment_2 ) ) )
            // InternalMyDsl.g:637:1: ( ( rule__Videoseq__UrlAssignment_2 ) )
            {
            // InternalMyDsl.g:637:1: ( ( rule__Videoseq__UrlAssignment_2 ) )
            // InternalMyDsl.g:638:2: ( rule__Videoseq__UrlAssignment_2 )
            {
             before(grammarAccess.getVideoseqAccess().getUrlAssignment_2()); 
            // InternalMyDsl.g:639:2: ( rule__Videoseq__UrlAssignment_2 )
            // InternalMyDsl.g:639:3: rule__Videoseq__UrlAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Videoseq__UrlAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getVideoseqAccess().getUrlAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Videoseq__Group__2__Impl"


    // $ANTLR start "rule__VideoGen__VidsAssignment_2"
    // InternalMyDsl.g:648:1: rule__VideoGen__VidsAssignment_2 : ( ruleVideoSequence ) ;
    public final void rule__VideoGen__VidsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:652:1: ( ( ruleVideoSequence ) )
            // InternalMyDsl.g:653:2: ( ruleVideoSequence )
            {
            // InternalMyDsl.g:653:2: ( ruleVideoSequence )
            // InternalMyDsl.g:654:3: ruleVideoSequence
            {
             before(grammarAccess.getVideoGenAccess().getVidsVideoSequenceParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleVideoSequence();

            state._fsp--;

             after(grammarAccess.getVideoGenAccess().getVidsVideoSequenceParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__VidsAssignment_2"


    // $ANTLR start "rule__Mandatory__VidAssignment_1"
    // InternalMyDsl.g:663:1: rule__Mandatory__VidAssignment_1 : ( rulevideoseq ) ;
    public final void rule__Mandatory__VidAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:667:1: ( ( rulevideoseq ) )
            // InternalMyDsl.g:668:2: ( rulevideoseq )
            {
            // InternalMyDsl.g:668:2: ( rulevideoseq )
            // InternalMyDsl.g:669:3: rulevideoseq
            {
             before(grammarAccess.getMandatoryAccess().getVidVideoseqParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            rulevideoseq();

            state._fsp--;

             after(grammarAccess.getMandatoryAccess().getVidVideoseqParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__VidAssignment_1"


    // $ANTLR start "rule__Optional__VidAssignment_1"
    // InternalMyDsl.g:678:1: rule__Optional__VidAssignment_1 : ( rulevideoseq ) ;
    public final void rule__Optional__VidAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:682:1: ( ( rulevideoseq ) )
            // InternalMyDsl.g:683:2: ( rulevideoseq )
            {
            // InternalMyDsl.g:683:2: ( rulevideoseq )
            // InternalMyDsl.g:684:3: rulevideoseq
            {
             before(grammarAccess.getOptionalAccess().getVidVideoseqParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            rulevideoseq();

            state._fsp--;

             after(grammarAccess.getOptionalAccess().getVidVideoseqParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__VidAssignment_1"


    // $ANTLR start "rule__Alternatives__IdAssignment_1"
    // InternalMyDsl.g:693:1: rule__Alternatives__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__Alternatives__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:697:1: ( ( RULE_ID ) )
            // InternalMyDsl.g:698:2: ( RULE_ID )
            {
            // InternalMyDsl.g:698:2: ( RULE_ID )
            // InternalMyDsl.g:699:3: RULE_ID
            {
             before(grammarAccess.getAlternativesAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAlternativesAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__IdAssignment_1"


    // $ANTLR start "rule__Alternatives__VidsAssignment_3"
    // InternalMyDsl.g:708:1: rule__Alternatives__VidsAssignment_3 : ( rulevideoseq ) ;
    public final void rule__Alternatives__VidsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:712:1: ( ( rulevideoseq ) )
            // InternalMyDsl.g:713:2: ( rulevideoseq )
            {
            // InternalMyDsl.g:713:2: ( rulevideoseq )
            // InternalMyDsl.g:714:3: rulevideoseq
            {
             before(grammarAccess.getAlternativesAccess().getVidsVideoseqParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            rulevideoseq();

            state._fsp--;

             after(grammarAccess.getAlternativesAccess().getVidsVideoseqParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__VidsAssignment_3"


    // $ANTLR start "rule__Videoseq__IdAssignment_1"
    // InternalMyDsl.g:723:1: rule__Videoseq__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__Videoseq__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:727:1: ( ( RULE_ID ) )
            // InternalMyDsl.g:728:2: ( RULE_ID )
            {
            // InternalMyDsl.g:728:2: ( RULE_ID )
            // InternalMyDsl.g:729:3: RULE_ID
            {
             before(grammarAccess.getVideoseqAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getVideoseqAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Videoseq__IdAssignment_1"


    // $ANTLR start "rule__Videoseq__UrlAssignment_2"
    // InternalMyDsl.g:738:1: rule__Videoseq__UrlAssignment_2 : ( RULE_STRING ) ;
    public final void rule__Videoseq__UrlAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:742:1: ( ( RULE_STRING ) )
            // InternalMyDsl.g:743:2: ( RULE_STRING )
            {
            // InternalMyDsl.g:743:2: ( RULE_STRING )
            // InternalMyDsl.g:744:3: RULE_STRING
            {
             before(grammarAccess.getVideoseqAccess().getUrlSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getVideoseqAccess().getUrlSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Videoseq__UrlAssignment_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x000000000001C000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000000001C002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000020L});

}